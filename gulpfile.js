const {src, dest, watch, parallel, series} = require('gulp');

const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');

function minifyImages(){
  return src('src/img/**')
  .pipe(imagemin())
  .pipe(dest('dist/img'));
}

function skripts() {
  return src('src/js/main.js')
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(dest('dist/js'))
  .pipe(browserSync.stream())
}

function styles() {
  return src('src/scss/*.scss')
  .pipe(autoprefixer({overrdeBrowserslist: ['last 3 version']}))
  .pipe(concat('style.min.css'))
  .pipe(scss({outputStyle: 'compressed'}))
  .pipe(dest('dist/css'))
  .pipe(browserSync.stream())
}

function watching () {
  watch(['src/scss/style.scss'], styles)
  watch(['src/js/main.js'], skripts)
  watch(['index.html']).on('change', browserSync.reload)
}

function browsersync(){
  browserSync.init({
    server: {
      baseDir:"./"
    }
  });
}

function cleanDist() {
  return src('dist', {allowEmpty: true})
  .pipe(clean())
}

function cleanDistJs() {
  return src('dist/js', {allowEmpty: true})
  .pipe(clean())
}

function cleanDistCss() {
  return src('dist/css', {allowEmpty: true})
  .pipe(clean())
}

function cleanDistImg() {
  return src('dist/img', {allowEmpty: true})
  .pipe(clean())
}

function build(){
  return src([
    'dist/css/style.min.css',
    'dist/js/main.min.js',
    'dist/img/**',
    'index.html'
  ], {base : './'})
  .pipe(dest('dist'))
}

exports.styles = styles;
exports.skripts = skripts;
exports.watching = watching;
exports.browsersync = browsersync;

exports.build = series(cleanDist, styles, skripts, minifyImages, build, cleanDistJs, cleanDistCss, cleanDistImg);
exports.default = series(cleanDist, parallel(styles, skripts, minifyImages, watching, browsersync));